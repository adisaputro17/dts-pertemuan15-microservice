package main

import (
    "io/ioutil"
	"log"
	_ "github.com/go-sql-driver/mysql"
    "net/http"
    "github.com/gorilla/mux"
    "encoding/json"
)

type Product []struct{
	Product_id int `json:"product_id"`
	Product_name string `json:"product_name"`
	Product_price int `json:"product_price"`
}

type Ongkir struct {
    ProvinceID string
    Province string 
    CityID   string
    CityName string
}

func getDataServer1(w http.ResponseWriter, r *http.Request){
    url := "http://34.235.136.231:5001/getOngkir"
    w.Header().Set("Content-Type", "application/json")
    req, _ := http.NewRequest("GET", url, nil)
    res, _ := http.DefaultClient.Do(req)
    if res.Body != nil {
		defer res.Body.Close()
	}
    body, _ := ioutil.ReadAll(res.Body)

    //fmt.Println(res)
    //fmt.Println(string(body))
    var ongkir Ongkir
    json.Unmarshal(body, &ongkir)
    /*ongkir.ProvinceID = ongkir.ProvinceID
    ongkir.Province = ongkir.Province
    ongkir.CityName = ongkir.CityID
    ongkir.CityID = ongkir.CityName*/
    json.NewEncoder(w).Encode(ongkir)
}

func getDataServer2(w http.ResponseWriter, r *http.Request){
    url := "http://3.88.163.71:5002/getProduct"
    w.Header().Set("Content-Type", "application/json")
    req, _ := http.NewRequest("GET", url, nil)
    res, _ := http.DefaultClient.Do(req)
    if res.Body != nil {
		defer res.Body.Close()
	}
    body, _ := ioutil.ReadAll(res.Body)

    //fmt.Println(res)
    //fmt.Println(string(body))
    var products Product
    json.Unmarshal(body, &products)
    json.NewEncoder(w).Encode(products)
}

func main() {
    router := mux.NewRouter()
    router.HandleFunc("/getDataServer1",getDataServer1).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API product ke /getproducts
    router.HandleFunc("/getDataServer2",getDataServer2).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API product ke /getproducts
    http.Handle("/",router)
    log.Println("starting web server at http://localhost:5000/")
    log.Fatal(http.ListenAndServe(":5000",router))
    
    //Adhi Dwi Saputro
}